package org.brit.pages;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class SuccessLoginPage {
    public SuccessLoginPage checkPageIsLoaded() {
        $("#flash.flash.success").should(visible, text("You logged into a secure area!"));
        $("a.button").should(text("Logout"));
        return this;
    }

    public FormAuthPage logout() {
        $(".radius").click();
        $("#flash.flash.success").shouldHave(text("You logged out of the secure area!"));
        return new FormAuthPage();
    }
}
