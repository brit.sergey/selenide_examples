package org.brit.pages;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class FormAuthPage {
    public FormAuthPage enterName(String name) {
        $("#username").val(name);
        return this;
    }

    public FormAuthPage enterPass(String pass) {
        $("#password").val(pass);
        return this;
    }

    public <T> T submit(Class<T> returnedClass) {
        $(".radius").click();
        try {
            return returnedClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public FormAuthPage errorMessageShouldBeVisible() {
        $("#flash.flash.error").should(visible, text("Your username is invalid!"));
        return this;
    }
}
