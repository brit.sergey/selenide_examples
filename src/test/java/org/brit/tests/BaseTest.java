package org.brit.tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

//@Listeners(ScreenShooter.class)
public class BaseTest {

    @BeforeSuite(alwaysRun = true)
    public void beforeSuiteMethod() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        //capabilities.setPlatform(Platform.VISTA);
//        capabilities.setVersion("83.0");
//        capabilities.setCapability("screenResolution", "1280x1024");
//        capabilities.setCapability("enableVNC", true);
//        capabilities.setCapability("enableVideo", false);

        Configuration.browserCapabilities = capabilities;

        Configuration.remote = "http://192.168.31.108:8444/wd/hub";
        Configuration.timeout = 15000;
        Configuration.baseUrl = "http://the-internet.herokuapp.com";
        Configuration.startMaximized = true;
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
    }
}
