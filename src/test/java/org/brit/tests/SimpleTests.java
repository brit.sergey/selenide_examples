package org.brit.tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.proxy.SelenideProxyServer;
import org.apache.commons.io.FileUtils;
import org.brit.pages.FormAuthPage;
import org.brit.pages.SuccessLoginPage;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getSelenideProxy;

public class SimpleTests extends BaseTest {
    @Test
    public void hoverOnImage() {
        open("/hovers");
        ElementsCollection collection = $$(".figure");
        for (int i = 0; i < collection.size(); i++) {
            SelenideElement element = collection.get(i);
            element.$(".figcaption h5").shouldNotBe(visible);
            element.hover();
            element.$(".figcaption h5").shouldBe(visible).shouldHave(text("name: user" + (i + 1)));
            element.$(".figcaption a").shouldBe(visible).shouldHave(text("View profile"));
        }
    }

    @Test
    public void dropDownTest() {
        open("/dropdown");
        $("#dropdown").selectOptionByValue("1");
        $("#dropdown").shouldHave(text("Option 1"));
        $("#dropdown").selectOption("Option 2");
        $("#dropdown").shouldHave(text("Option 2"));
    }

    @Test
    public void checkBoxText() {
        open("/checkboxes");
        $("#checkboxes").$$("input").first().shouldNotBe(checked);
        $("#checkboxes").$$("input").last().shouldBe(checked);
        $("#checkboxes").$$("input").first().click();
        sleep(500);
        $("#checkboxes").$$("input").first().is(not(checked));
    }

    @Test
    public void sliderTest() {
        Configuration.fastSetValue = true;
        open("/horizontal_slider");
        $("input").val("3");
        $("#range").shouldHave(text("3"));
        Configuration.fastSetValue = false;
    }

    @Test
    public void keyPressTest() {
        open("/key_presses");
        actions().sendKeys(Keys.ALT).build().perform();
        $x("//*[@id='result']").shouldHave(text("You entered: " + Keys.ALT.name()));
        actions().sendKeys(Keys.ENTER).build().perform();
        $x("//*[@id='result']").shouldHave(text("You entered: " + Keys.ENTER.name()));
    }

    @Test(priority = 20)
    public void waitForTest() {
        open("/dynamic_loading/1");
        $("#start button").shouldBe(visible).click();
        $("#loading").shouldBe(visible);
        $("#finish").shouldBe(visible).shouldHave(text("Hello world!"));
    }

    @Test
    public void waitForTest2() {
        open("/dynamic_controls");
        $("#checkbox-example button").shouldHave(text("Remove"));
        $("#checkbox").shouldBe(visible).is(not(selected));
        $("#checkbox-example button").click();
        $("#loading").shouldBe(visible).shouldNotBe(visible);
        $("#checkbox").shouldNot(visible);
        $("#checkbox-example button").shouldHave(text("Add")).click();
        $("#checkbox").shouldBe(visible).is(not(selected));
    }

    @Test(priority = 10)
    public void uploadTest() {
        open("/upload");
        File fileToUpload = new File(getClass().getClassLoader().getResource("text.txt").getPath());
        $("#file-upload").uploadFile(fileToUpload);
        $("form").submit();
        $("#uploaded-files").shouldHave(text(fileToUpload.getName()));
    }

    @Test(enabled = false)
    public void downloadTest() throws IOException {
        open("/upload");
        SelenideProxyServer selenideProxy = getSelenideProxy();
        selenideProxy.addRequestFilter(
                "proxy-usages.request",
                (request, contents, messageInfo) -> {
                    request.headers().add("Set-Cookie", "someCookie=cookieF");
                    return null;
                }
        );
        selenideProxy.addResponseFilter(
                "proxy-usages.response",
                (request, contents, messageInfo) -> {
                    System.out.println(request.headers().get("Set-Cookie"));
                }
        );
        File fileToUpload = $("#file-upload").uploadFromClasspath("text.txt");
        $("#file-submit").click();
        $("#uploaded-files").shouldHave(text(fileToUpload.getName()));
        open("/download");
        File downloadedFile = $$("a").find(text(fileToUpload.getName())).download();
        Assert.assertTrue(FileUtils.contentEquals(fileToUpload, downloadedFile));
        getSelenideProxy().addResponseFilter(
                "proxy-usages.request",
                (request, contents, messageInfo) -> {
                    System.out.println(request.headers().get("Cookie"));
                }
        );
    }

    @Test(enabled = false)
    public void proxyTest() {
        open("/basic_auth", "", "admin", "admin");
        $("#content").should(visible, text("Congratulations! You must have the proper credentials."));
    }

    @Test
    public void alertTest() {
        open("/javascript_alerts");
        $(byText("Click for JS Alert")).click();
        confirm("I am a JS Alert");
        $("#result").should(text("You successfuly clicked an alert"));
        $(byText("Click for JS Confirm")).click();
        confirm("I am a JS Confirm");
        $("#result").should(text("You clicked: Ok"));
        $(byText("Click for JS Confirm")).click();
        dismiss("I am a JS Confirm");
        $("#result").should(text("You clicked: Cancel"));
        $(byText("Click for JS Prompt")).click();
        prompt("SomeText");
        $("#result").should(text("You entered: SomeText"));
        $(byText("Click for JS Prompt")).click();
        dismiss();
        $("#result").should(text("You entered: null"));
    }

    @Test(priority = 1000)
    public void dNdTest() {
        open("http://www.pureexample.com/jquery-ui/basic-droppable.html");
        switchTo().frame("ExampleFrame-94");
        $(".square.ui-draggable").dragAndDropTo(".squaredotted.ui-droppable");
        sleep(2000);
        $(".squaredotted.ui-droppable").parent().$("#info").shouldHave(text("dropped!"));
        $(".square.ui-draggable").dragAndDropTo("#form1");
        sleep(2000);
        $(".squaredotted.ui-droppable").parent().$("#info").shouldHave(text("moving out!"));
    }

    @Test
    public void iFrameTest() {
        open("/iframe");
        switchTo().frame("mce_0_ifr");
        $("#tinymce").setValue("<br><br><h1>Some text!!!</h1>");
        sleep(3000);
    }

    @Test
    public void checkLoginLogout() {
        //Incorrect login
        open("/login");
        new FormAuthPage()
                .enterName("blahBlah")
                .enterPass("BlahBlah")
                .submit(FormAuthPage.class)
                .errorMessageShouldBeVisible()
                // Correct Login
                .enterName("tomsmith")
                .enterPass("SuperSecretPassword!")
                .submit(SuccessLoginPage.class)
                .checkPageIsLoaded()
                .logout();
    }
}
